package main

import (
	"github.com/line/line-bot-sdk-go/linebot"
	"log"
)

var bot *linebot.Client

func main() {
	var err error
	bot, err = linebot.New(
		"secret",
		"token",
	)
	if err != nil {
		log.Fatal(err)
	}

	container, err := getContainer()
	if err != nil {
		log.Fatal(err)
	}

	if _, err := bot.PushMessage(
		"lineID",
		linebot.NewFlexMessage("test", container),
	).Do(); err != nil {
		log.Fatal(err)
	}
	return
}

//　jsonデータでメッセージの内容を保持->linebot.FlexContainerに変換
func getContainer() (linebot.FlexContainer, error) {
	jsonData := []byte(`
	{
	  "type": "bubble",
	  "hero": {
	    "type": "image",
	    "url": "https://scdn.line-apps.com/n/channel_devcenter/img/fx/01_1_cafe.png",
	    "size": "full",
	    "aspectRatio": "20:13",
	    "aspectMode": "cover",
	    "action": {
	      "type": "uri",
	      "uri": "http://linecorp.com/"
	    }
	  },
	  "body": {
	    "type": "box",
	    "layout": "vertical",
	    "contents": [
	      {
	        "type": "text",
	        "text": "Brown Cafe",
	        "weight": "bold",
	        "size": "xl"
	      },
	      {
	        "typess": "box",
	        "layout": "baseline",
	        "margin": "md",
	        "contents": [
	          {
	            "type": "icon",
	            "size": "sm",
	            "url": "https://scdn.line-apps.com/n/channel_devcenter/img/fx/review_gold_star_28.png"
	          },
	          {
	            "type": "icon",
	            "size": "sm",
	            "url": "https://scdn.line-apps.com/n/channel_devcenter/img/fx/review_gold_star_28.png"
	          },
	          {
	            "type": "icon",
	            "size": "sm",
	            "url": "https://scdn.line-apps.com/n/channel_devcenter/img/fx/review_gold_star_28.png"
	          },
	          {
	            "type": "icon",
	            "size": "sm",
	            "url": "https://scdn.line-apps.com/n/channel_devcenter/img/fx/review_gold_star_28.png"
	          },
	          {
	            "type": "icon",
	            "size": "sm",
	            "url": "https://scdn.line-apps.com/n/channel_devcenter/img/fx/review_gray_star_28.png"
	          },
	          {
	            "type": "text",
	            "text": "4.0",
	            "size": "sm",
	            "color": "#999999",
	            "margin": "md",
	            "flex": 0
	          }
	        ]
	      },
	      {
	        "type": "box",
	        "layout": "vertical",
	        "margin": "lg",
	        "spacing": "sm",
	        "contents": [
	          {
	            "type": "box",
	            "layout": "baseline",
	            "spacing": "sm",
	            "contents": [
	              {
	                "type": "text",
	                "text": "Place",
	                "color": "#aaaaaa",
	                "size": "sm",
	                "flex": 1
	              },
	              {
	                "type": "text",
	                "text": "Miraina Tower, 4-1-6 Shinjuku, Tokyo",
	                "wrap": true,
	                "color": "#666666",
	                "size": "sm",
	                "flex": 5
	              }
	            ]
	          },
	          {
	            "type": "box",
	            "layout": "baseline",
	            "spacing": "sm",
	            "contents": [
	              {
	                "type": "text",
	                "text": "Time",
	                "color": "#aaaaaa",
	                "size": "sm",
	                "flex": 1
	              },
	              {
	                "type": "text",
	                "text": "10:00 - 23:00",
	                "wrap": true,
	                "color": "#666666",
	                "size": "sm",
	                "flex": 5
	              }
	            ]
	          }
	        ]
	      }
	    ]
	  },
	  "footer": {
	    "type": "box",
	    "layout": "vertical",
	    "spacing": "sm",
	    "contents": [
	      {
	        "type": "button",
	        "style": "link",
	        "height": "sm",
	        "action": {
	          "type": "uri",
	          "label": "CALL",
	          "uri": "https://linecorp.com"
	        }
	      },
	      {
	        "type": "button",
	        "style": "link",
	        "height": "sm",
	        "action": {
	          "type": "uri",
	          "label": "WEBSITE",
	          "uri": "https://linecorp.com"
	        }
	      },
	      {
	        "type": "box",
	        "layout": "vertical",
	        "contents": [],
	        "margin": "sm"
	      }
	    ],
	    "flex": 0
	  }
	}`)
	container, err := linebot.UnmarshalFlexMessageJSON(jsonData)
	if err != nil {
		return nil, err
	}
	return container, nil
}

//　linebot.FlexContainerを実装しているlinebot.BubbleContainerでmessage内容を保持
func getContainer2() linebot.FlexContainer {
	return &linebot.BubbleContainer{
		Type: linebot.FlexContainerTypeBubble,
		Body: &linebot.BoxComponent{
			Type:   linebot.FlexComponentTypeBox,
			Layout: linebot.FlexBoxLayoutTypeHorizontal,
			Contents: []linebot.FlexComponent{
				&linebot.TextComponent{
					Type: linebot.FlexComponentTypeText,
					Text: "Hello,",
				},
				&linebot.TextComponent{
					Type: linebot.FlexComponentTypeText,
					Text: "World!",
				},
			},
		},
	}
}
